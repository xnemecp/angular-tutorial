### Install Node and npm
- Angular version 9 requires minimal version Node.js 10.13
- https://nodejs.org/en/download/

### Install the Angular CLI
- https://angular.io/guide/setup-local
- npm install -g @angular/cli

### Create a workspace and initial application
- https://angular.io/cli/new
- ng new angular-tutorial --routing=true --style=less

### Add bootstrap
- npm install bootstrap --save
- add "node_modules/bootstrap/dist/css/bootstrap.min.css" to angualr.json "style" property 

### Ng generate
- https://angular.io/cli/generate
